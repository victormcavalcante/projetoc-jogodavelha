#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

//Entrada

char jogo[3][3]; // Matriz utilizada para funcionalidade do jogo
int l, c; // Variáveis Utilizadas para Linha e Coluna

//Função Utilizada para a abertura do menu principal, nas quais temos as opções de iniciar o jogo, ver os créditos, e sair;
int menu() {
	int op;
	
	printf("Jogo da Velha\n\n\n");
	
	printf("MENU\n");
	printf("\n1 - INICIAR JOGO \n2 - CRÉDITOS\n3 - SAIR\n\n");
	printf("Opção que deseja escolher: ");
	scanf("%d", &op);
	
	switch (op){
		case 1:
			return op;
			break;
		case 2:
			return op;
			break;
		
		case 3:
			return op;
			break;
		
		default:
			printf("OPÇÃO Digitada Inválida!!");
			break;

	system("cls");
	}
}

// Função utilizada para iniciar a matriz com espaços;
void criarMatriz(){
    for(l = 0; l < 3; l++){
        for(c = 0; c < 3; c++)
            jogo[l][c] = ' ';
    }
    
}

// Função que imprime o desenho do jogo da velha (#) no terminal;
void criarTabuleiro(){
	printf("Jogo da velha\n\n");
    printf("\n\n\t 0   1   2\n\n");
    for(l = 0; l < 3; l++){
        for(c = 0; c < 3; c++){
            if(c == 0)
                printf("\t");
            printf(" %c ", jogo[l][c]);
            if(c < 2)
                printf(":");
            if(c == 2)
                printf("   %d", l);
                
        }
        printf("\n");
        if(l < 2)
            printf("\t ..:...:..\n");
    }
}

// Funções utilizadas para verificar se houve vitória dos jogadores, seja por linhas, colunas ou diagonais!
int vitoriaPorLinha(int l, char c){
    if(jogo[l][0] == c && jogo[l][1] == c && jogo[l][2] == c)
        return 1;
    else
        return 0;
}
int vitoriaPorLinhas(char c){
    int ganhou = 0;
    for(l = 0; l < 3; l++){
        ganhou += vitoriaPorLinha(l, c);
    }
    return ganhou;
}
int vitoriaPorColuna(int c, char j){
    if(jogo[0][c] == j && jogo[1][c] == j && jogo[2][c] == j)
        return 1;
    else
        return 0;
}
int vitoriaPorColunas(char j){
    int ganhou = 0;
    for(c = 0; c < 3; c++){
        ganhou += vitoriaPorColuna(c, j);
    }
    return ganhou;
}
int vitoriaDiagonalPrincipal(char c){
    if(jogo[0][0] == c && jogo[1][1] == c && jogo[2][2] == c)
        return 1;
    else
        return 0;
}

int vitoriaDiagonalSecundaria(char c){
    if(jogo[0][2] == c && jogo[1][1] == c && jogo[2][0] == c)
        return 1;
    else
        return 0;
}

// Função que verifica se a casa indicada pelo jogador é valida!
int validaJogada(int l, int c){
    if(l >= 0 && l < 3 && c >= 0 && c < 3 && jogo[l][c] == ' ')
        return 1;
    else
        return 0;
}

// Função que irá ler o local exato em que o jogador deseja efetuar sua jogada, por meio do index da matriz
void jogada(char j){
    int linha, coluna;

    printf("\n\nInsira a linha que deseja efetuar sua jogada : ");
    scanf("%d", &linha);
    printf("\n\nInsira a coluna que deseja efetuar sua jogada: ");
    scanf("%d", &coluna);

    while(validaJogada(linha, coluna) == 0){
        printf("\n\nPosi��o selecionada inv�lida! Digite outra linha e coluna, preste aten��o! ");
        printf("\n\nInsira a linha que deseja efetuar sua jogada : ");
    	scanf("%d", &linha);
    	printf("\n\nInsira a coluna que deseja efetuar sua jogada: ");
    	scanf("%d", &coluna);
    }
    jogo[linha][coluna] = j;
}

// Função que verifica a quantidade de casas vazias que ainda podem ser jogadas
int quantidadeVazias(){
    int quantidade = 0;

    for(l = 0; l < 3; l++){
        for(c = 0; c < 3; c++)
            if(jogo[l][c] == ' ')
                quantidade++;
    }
    return quantidade;
}

// Função que inicializa o jogo da velha e apaga do terminal o tabuleiro antigo, deixando somente o mais atualizado!
void jogar(){
    int jogador = 1, vitoriaX = 0, vitoria0 = 0;
    char player1 = 'X', player2 = 'O';

    do{
        criarTabuleiro();
        if(jogador == 1){
            jogada(player1);
            jogador++;
            vitoriaX += vitoriaPorLinhas(player1);
            vitoriaX += vitoriaPorColunas(player1);
            vitoriaX += vitoriaDiagonalPrincipal(player1);
            vitoriaX += vitoriaDiagonalSecundaria(player1);
            system("cls");
        }
        else{
            jogada(player2);
            jogador = 1;
            vitoria0 += vitoriaPorLinhas(player2);
            vitoria0 += vitoriaPorColunas(player2);
            vitoria0 += vitoriaDiagonalPrincipal(player2);
            vitoria0 += vitoriaDiagonalSecundaria(player2);
            system("cls");
        }
    }while(vitoriaX == 0 && vitoria0 == 0 && quantidadeVazias() > 0);

    criarTabuleiro();
    

    if(vitoria0 == 1)
        printf("\nParabens Jogador 2 (O). Você ganhou!!!\n");
    else if(vitoriaX == 1)
        printf("\nParabens Jogador 1 (X). Você ganhou!!!\n");
    else
        printf("\nDeu velha! Que venha o desempate!\n");
}

int main(){
	
	//ENTRADA
    int op; // opção selecionada no menu
    int novaPartida; // opção para saber se o jogador deseja realizar nova partida
    setlocale(0, "Portuguese");
    
    
	op = menu();
	system("cls");
	
	//PROCESSAMENTO E SA�DA
	/*
	O usuário, após a escolha da opção, visualizara o jogo, os créditos ou sair do jogo. 
	Caso a opção seja 1,  surgirá na tela um desenho do jogo da velha, na qual iniciando pelo jogador 1, será perguntado qual a posição que este gostaria de efetuar sua jogada
	(escolhendo primeiramente a linha e apos isso a coluna que indicam a posição exata na matriz que será colocado o símbolo).
	O jogo é finalizado a partir do momento em que forma-se uma linha, coluna ou diagonal completas do mesmo simbolo (X para jogador 1 e O para jogador 2), resultando em vitória para um ou outro, ou quando todas as 9 jogadas forem efetuadas, resultando em um empate.
	Após realizada a verifição da vitória ou empate entre os jogadores, é chamada a função que pergunta se gostariam de realizar uma nova partida. 
	Caso seja a opção 2, a saída será a identifição dos integrantes do grupo(CREDITOS).
	Caso a opção seja 3, o programa irá finalizar.
	*/
	
	if(op == 1){			
    	do{
        	criarMatriz();
        	jogar();
        	printf("\nDigite 1 caso queira revanche: ");
        	scanf("%d", &novaPartida);
        	system("cls");
    }while(novaPartida == 1);
}
	else if(op == 2){
		printf("\n\n\nProjeto de jogo da velha realizado por: Agnélio Junior, Lucas Eduardo, Lucas Rosemberg e Victor Medeiros\n\n\n");
	}
    
}
